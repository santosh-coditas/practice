const user1 = {
    firstName : "Mukund",
    age: 22,   
    about:function(){
        console.log(this.firstName, this.age,);
    }
}
//don't write code like this
const function1=user1.about;

function1()
//use this logic

const function2=user1.about.bind(user1);
function2()