//here we are using method of another object 
const user1 = {
    firstName : "Mukund",
    age: 22,   
    about:function(){
        console.log(this.firstName, this.age,);
    }
}
const user2 = {
    firstName : "XQF",
    age: 23,
    
}
user1.about.call(user2)

//apply
function about(){
    console.log(this.firstName, this.age,);
}
about.apply(user2)

//bind
//it will return the function
const funct=about.bind(user2)
