//key value pair 
const person = new Map();
person.set('firstName', 'Mukund');
person.set('age', 22);
person.set(1,'one');
// person.set([1,2,3],'onetwothree');
// person.set({1: 'one'},'onetwothree');
console.log(person);
console.log(person.get(1));

for(let key of person.keys()){
    console.log(key);
}

for(let value of person.value){
  
    console.log(value)
}