let fruit=["apple","mango","graps"];
console.log(fruit);
// fruit.push("Banana");//it will add from last
console.log(fruit);
// console.log(fruit.pop());//it will remove from last
let popedFruit=fruit.pop();
console.log(fruit);
console.log(popedFruit);

fruit.unshift("banana");//add from start
console.log(fruit);

let removedFruit=fruit.shift(); //remove from start
console.log(fruit);

//push and pop are fast