let num1="5";
let num2=44;
console.log(num1>=num2);

//== vs ===
console.log(num1==num2);//it will check only value not datatype
console.log(num1===num2);//it will check datatype also

//!=vs !==

console.log(num1!=num2);//it will check only value not datatype

console.log(num1!==num2);//it will check datatype also