
//function to create objects
function createUser(firstName, lastName, email, age){
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.about = userMethods.about;
    return user;
}


const userMethods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
   
}

//users
const user1 = createUser('Mukund', 'Wagh', 'mukund@gmail.com', 22,);
const user2 = createUser('Xqf', 'Hydra', 'hydra@gmail.com', 23,);

console.log(user1.about());
console.log(user2.about());