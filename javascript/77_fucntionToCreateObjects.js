//create multiple objects using function

function createUser(firstName, lastName, email, age){
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
   
    user.about = function(){
        return `${this.firstName} ${this.lastName} is ${this.age} years old.`;
    };
    
    return user;
}

const user1 = createUser('Mukund', 'Wagh', 'mukund@gmail.com', 22);
console.log(user1);


const about = user1.about();
console.log(about);