

// function sum(num1, num2, num3){
//     return num1 + num2 + num3;
// }

const sum = function(num1, num2, num3){
    return num1 + num2 + num3;
}

function even(num){
    return num % 2 === 0;
}

console.log(even(46748329));


function find(arr, target){
    for(let i = 0; i<arr.length; i++){
        if(arr[i]===target){
            return i;
        }
    }
    return -1;
}

const arr=[1,2,3,4,5,6]
console.log(find(arr,4));