// reduce(accumulator, currentValue)
//next time value of accumulator = value return by function


const arr=[
    {
        name:"Mukund",salary:7000

    },
    {
        name:"Hydra", salary:8289
    }
    ,{
        name:"XQF",salary:7899
    }
]
const sumOfSalaries=arr.reduce((totalSalary,currentSalary)=>{
    return totalSalary+currentSalary.salary;
},0)

//here 0 is default value of totalSalary
console.log(sumOfSalaries)
