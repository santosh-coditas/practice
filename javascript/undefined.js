let firstName;
console.log(typeof firstName);
firstName="Santosh";
console.log(typeof firstName,firstName);


//null

let myVariable=null;
console.log(myVariable);
console.log(typeof myVariable);
myVariable="null";

console.log(typeof null);
//bug, error it shows that null
// is object but it is not 
//we have to take care of that.


//BigInt
let myNumber=123;
console.log(myNumber);
console.log(Number.MAX_SAFE_INTEGER);//it will shows the maximum value for integer


myNumber=BigInt(12547456456475672743973);
let same=437265836425876n;//another way to declare big int
console.log(myNumber+same);

//you will not do operation on bigint and simple int
