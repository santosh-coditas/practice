let num1=7;
let num2=num1;
console.log("Value of num1 is",num1);
console.log("Value of num2 is",num2);
num1++;
console.log("Value of num2 is",num1);
console.log("Value of num1 is",num1);
console.log("Value of num2 is",num2);

//referance type

let array1=["item1","item2"];
let array2=array1;

console.log("Value of array1 is",array1);
console.log("Value of array2 is",array2);
array1.push("item3");
console.log("After pushing element in array1");
console.log("Value of array2 is",array1);
console.log("Value of array2 is",array2);